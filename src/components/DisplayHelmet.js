import React, { Component } from 'react';
import { Col, Row} from 'react-bootstrap';

class Index extends Component {
  state = {
  }
  render() { 
    const {data}=this.props;
    console.log(data)
    return ( 
      <Row className='bg-white my-5'>
        <Col>
          <img className='img-size mx-5' src={data.img} alt={data.name}/>
        </Col>
        <Col>
          <p className='name'>{data.name}</p>
          <p className='text-30'>Price: {data.price}</p>
          <p className='text-30'>Quantity: {data.quantity}</p>
          <p className='text-30'><i class="fa fa-cart-plus mr-5" aria-hidden="true"></i><i class="fa fa-list-alt ml-5" aria-hidden="true"></i></p>
        </Col>
      </Row>
    );
  }
}
 
export default Index;